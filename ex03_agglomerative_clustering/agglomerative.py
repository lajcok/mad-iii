import sys

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


def euclidean_distance(points):
    """
    Compute euclidean distance matrix
    :param points: 2D array of shape `(instances, dimensions)`
    :return: Returns distance matrix in 2D array
    """
    squared = np.sum(points ** 2, axis=-1)
    xy_multiplied = np.dot(points, points.T)
    dist_core = -2 * xy_multiplied + squared + squared[:, np.newaxis]
    dist_core[dist_core < 0] = 0
    return np.sqrt(dist_core)


def average_linkage(similarity_matrix, clusters):
    def _clusters_similarity(*abc):
        dim_masks = [
            np.zeros(len(similarity_matrix), dtype=bool)
            for _ in range(len(abc))
        ]
        for i, a in enumerate(abc):
            dim_masks[i][a] = True
        intersection = similarity_matrix[np.outer(*dim_masks)]
        return np.mean(intersection)

    return np.array([
        [
            _clusters_similarity(a, b)
            for b in clusters
        ]
        for a in clusters
    ])


def single_linkage(similarity_matrix, clusters):
    return np.array([
        [
            similarity_matrix[a][:, b].min()
            for b in clusters
        ]
        for a in clusters
    ])


def complete_linkage(similarity_matrix, clusters):
    return np.array([
        [
            similarity_matrix[a][:, b].max()
            for b in clusters
        ]
        for a in clusters
    ])


class AgglomerativeClustering:
    """
    Agglomerative Clustering algorithm
    """

    def __init__(
            self,
            similarity_matrix,
            treat_as_distance=False,
            linkage_fn=None,
            clusters=None,
            cluster_similarity=None,
    ):
        self.similarity_matrix = similarity_matrix
        # TODO store clusters as a vector to improve performance
        self.clusters = clusters or [
            [i] for i in range(len(similarity_matrix))
        ]
        self.cluster_similarity = cluster_similarity if cluster_similarity is not None else similarity_matrix
        self.treat_as_distance = treat_as_distance
        self.linkage_fn = linkage_fn or average_linkage

    def step(self):
        non_diagonal = self.cluster_similarity.copy()
        np.fill_diagonal(non_diagonal, np.inf if self.treat_as_distance else -np.inf)
        i, j = np.unravel_index(
            non_diagonal.argmin() if self.treat_as_distance else non_diagonal.argmax(),
            non_diagonal.shape
        )

        new_cluster = self.clusters[i] + self.clusters[j]
        next_clusters = [
            c if ci != i else new_cluster
            for ci, c in enumerate(self.clusters)
            if ci != j
        ]

        cluster_similarity = self.linkage_fn(self.similarity_matrix, next_clusters)

        return self.__class__(
            similarity_matrix=self.similarity_matrix,
            clusters=next_clusters,
            cluster_similarity=cluster_similarity,
            treat_as_distance=self.treat_as_distance,
            linkage_fn=self.linkage_fn,
        ), (i, j)

    def __next__(self):
        return self.step()

    def __iter__(self):
        step = self
        yield step, None
        while len(step.clusters) > 1:
            step, idx = next(step)
            yield step, idx


def main(dataset, linkage):
    data = pd.read_csv(dataset, sep=';', header=None)
    distance_matrix = euclidean_distance(data.to_numpy())

    agglomerative = AgglomerativeClustering(
        distance_matrix,
        treat_as_distance=True,
        linkage_fn={
            'single': single_linkage,
            'complete': complete_linkage,
        }[linkage],
    )

    step = None
    for i, (step, idx) in enumerate(agglomerative):
        print(i, idx, step.clusters)

        # naive stopping condition on the count of 3 clusters (tailored for dataset `clusters3.csv`)
        if len(step.clusters) <= 3:
            break

    cluster_map = np.empty(len(data), dtype=int)
    for i, cluster in enumerate(step.clusters):
        cluster_map[cluster] = i

    plt.scatter(data[0], data[1], c=cluster_map)
    plt.show()


if __name__ == '__main__':
    """
    to run, use: `python agglomerative.py <dataset> [<linkage type> single | complete]`
    """
    main(
        dataset=sys.argv[1],
        linkage=sys.argv[2] if len(sys.argv) >= 3 else 'complete',
    )
