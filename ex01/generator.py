from functools import reduce

import pandas as pd
import numpy as np
import itertools as it


def load_dat(file, columns=None):
    file = open(file, 'r')
    lines = file.readlines()
    data = pd.DataFrame(
        data=0,
        dtype=bool,
        index=range(1, 1 + len(lines)),
        columns=columns,
    )

    for i, line in zip(data.index, lines):
        for item in line.split(' '):
            try:
                idx = int(item)
                if idx not in data:
                    data[idx] = 0
                data.at[i, idx] = 1
            except ValueError:
                continue

    return data


def support(combo, data):
    combo_arr = np.array(combo) - 1
    subset = data.to_numpy()[:, combo_arr]
    column = subset.all(axis=-1)
    return column.sum() / len(column)


def apriori(data, min_support):
    # levels collector, iterative groups
    levels = []
    groups = [(c,) for c in data.columns]

    while len(groups) > 0:
        # compute support for current groups
        current_level = (
            (combo, support(combo, data))
            for combo in groups
        )
        # keep only sufficient support
        sufficient_groups = [
            (c, s)
            for c, s in current_level
            if s >= min_support
        ]
        levels.append(sufficient_groups)
        yield sufficient_groups

        # create combinations of level's desired length, union elements
        groups = list(filter(
            lambda t: len(t) == 1 + len(levels),
            (
                reduce(lambda acc, c: tuple(set(acc) | set(c)), combo)
                for combo in it.combinations((c for c, _ in sufficient_groups), r=2)
            )
        ))


def main():
    data = load_dat('chess.dat', columns=range(1, 1 + 75))

    for i, step in enumerate(apriori(data, min_support=.9)):
        print(f'{i + 1}:\t{step}')

    exit(0)


if __name__ == '__main__':
    main()
